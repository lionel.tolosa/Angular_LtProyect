import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-temas',
  templateUrl: './temas.component.html',
  styleUrls: ['./temas.component.css']
})
export class TemasComponent implements OnInit {
  serverId: Number = 10;
  serverStatus: String = 'offline';
  getServerStatus() {  // String Interpolation
    return this.serverStatus;
  }

  constructor() { }

  ngOnInit() {
  }

}
