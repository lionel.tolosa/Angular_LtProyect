import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor() {     // Colocar los metodos de las clases
  }

  ngOnInit() {  // Aqui va las logica de la aplicacion
  }
}
