import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app.routes';
import { FormComponent } from './form/form.component';
import { ImagesComponent } from './images/images.component';
import { TemasComponent } from './temas/temas.component';
import { ContactoComponent } from './contacto/contacto.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,   // Componentes
    HomeComponent, FormComponent, ImagesComponent, TemasComponent, ContactoComponent, HeaderNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule  // Modulos

  ],
  providers: [],  // Servicios y Modelos
  bootstrap: [AppComponent]
})
export class AppModule { }
