import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { ImagesComponent } from './images/images.component';
import { TemasComponent } from './temas/temas.component';
import { ContactoComponent } from './contacto/contacto.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'form', component: FormComponent },
  { path: 'images', component: ImagesComponent },
  { path: 'temas', component: TemasComponent },
  { path: 'contacto', component: ContactoComponent },
  { path: '**', pathMatch: 'full', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }



